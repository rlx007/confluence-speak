// import SectionMessage from "@atlaskit/section-message";
import React, { useEffect, useState } from "react";
import StopIcon from "@material-ui/icons/Stop";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import MicRecorder from "mic-recorder-to-mp3";
import axios from "axios";

const Mp3Recorder = new MicRecorder({ bitRate: 128 });

export default function HelloWorld(props) {
  console.log("props", props);
  const [isRecording, setisRecording] = useState(false);
  const [blobURL, setblobURL] = useState("");
  const [isBlocked, setisBlocked] = useState(false);
  const [audiobuffer, setaudiobuffer] = useState(null);

  useEffect(() => {
    console.log(audiobuffer);
    if (audiobuffer) {
      let formData = new FormData();
      formData.append("file", audiobuffer);
      formData.append("minorEdit", "true");
      axios
        .put(
          "https://my-plugin.atlassian.net/wiki/rest/api/content/14319617/child/attachment/",
          formData,
          {
            headers: {
              Authorization:
                "Basic dnMuZG9jZUBnbWFpbC5jb206UzZzem1wRGc4THB3Qnd6VUZ6SHlBRkVB",
              Accept: "application/json",
            },
          }
        )
        .then((res) => console.log(res))
        .then((err) => console.log(err));
    }
  }, [audiobuffer]);

  useEffect(() => {
    navigator.mediaDevices.getUserMedia(
      { audio: true },
      () => {
        console.log("Permission Granted");
        setisBlocked(false);
      },
      () => {
        console.log("Permission Denied");
        setisBlocked(true);
      }
    );
  }, []);

  const start = () => {
    if (isBlocked) {
      console.log("Permission Denied");
    } else {
      Mp3Recorder.start()
        .then(() => {
          setisRecording(true);
        })
        .catch((e) => console.error("starterror", e));
    }
  };

  const stop = () => {
    Mp3Recorder.stop()
      .getMp3()
      .then(([buffer, blob]) => {
        const blobURL = URL.createObjectURL(blob);
        // let reader = new FileReader();
        console.log(buffer);
        // reader.readAsDataURL(blob);
        // reader.onloadend = function () {
        //   const base64 = reader.result;
        //   console.log(base64);
        // };
        setaudiobuffer(buffer);
        setblobURL(blobURL);
        setisRecording(false);
      })
      .catch((e) => console.log(e));
  };

  return (
    <div className="container">
      {!isRecording ? (
        <div className="record-div icon-div" onClick={start}>
          <span>
            <FiberManualRecordIcon />
          </span>
        </div>
      ) : (
        <div className="stop-div icon-div" onClick={stop}>
          <span>
            <StopIcon />
          </span>
        </div>
      )}

      <audio controls="controls" src={blobURL} />
    </div>
  );
}
