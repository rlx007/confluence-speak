import React from "react";

var new_popup;
var buffer;
var blob;
var audiofile;
var params = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,
  width=600,height=300,left=100,top=100`;
var consolelog = () => {
  console.log("final value of blob in main", blob);
  console.log("file", audiofile);
  const rec = document.getElementById("audio-recorder");
  console.log("recorder", rec);
  AP.request({
    url: "/rest/api/content/28966913/child/attachment",
    type: "POST",
    contentType: "multipart/form-data",
    data: { file: audiofile, minorEdit: true },
    success: function (responseText) {
      console.log("success", responseText);
      AP.request({
        url: "/rest/api/content/28966913/child/attachment",
        type: "GET",
        success: function (response) {
          const json_data = JSON.parse(response);
          console.log(json_data);
          const audio_url =
            "https://my-plugin.atlassian.net/wiki" +
            json_data.results[0]._links.download;
          rec.setAttribute("src", audio_url);
          console.log(audio_url);
        },
        error: function (xhr, statusText, errorThrown) {
          console.log(arguments);
        },
      });
    },
    error: function (xhr, statusText, errorThrown) {
      console.log(arguments);
    },
  });
};

const AudioRecorder = () => {
  const openPopup = () => {
    new_popup = window.open("./hello.html", "popup", params);
  };
  return (
    <div>
      <div>hello this is</div>
      <button onClick={openPopup}>Open Popup</button>
      <audio src="" controls="true" id="audio-recorder"></audio>
    </div>
  );
};

export default AudioRecorder;
