// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"pdwg":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = HelloWorld;

var _react = _interopRequireWildcard(require("react"));

var _Stop = _interopRequireDefault(require("@material-ui/icons/Stop"));

var _FiberManualRecord = _interopRequireDefault(require("@material-ui/icons/FiberManualRecord"));

var _micRecorderToMp = _interopRequireDefault(require("mic-recorder-to-mp3"));

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

// import SectionMessage from "@atlaskit/section-message";
const Mp3Recorder = new _micRecorderToMp.default({
  bitRate: 128
});

function HelloWorld(props) {
  console.log("props", props);
  const [isRecording, setisRecording] = (0, _react.useState)(false);
  const [blobURL, setblobURL] = (0, _react.useState)("");
  const [isBlocked, setisBlocked] = (0, _react.useState)(false);
  const [audiobuffer, setaudiobuffer] = (0, _react.useState)(null);
  (0, _react.useEffect)(() => {
    console.log(audiobuffer);

    if (audiobuffer) {
      let formData = new FormData();
      formData.append("file", audiobuffer);
      formData.append("minorEdit", "true");

      _axios.default.put("https://my-plugin.atlassian.net/wiki/rest/api/content/14319617/child/attachment/", formData, {
        headers: {
          Authorization: "Basic dnMuZG9jZUBnbWFpbC5jb206UzZzem1wRGc4THB3Qnd6VUZ6SHlBRkVB",
          Accept: "application/json"
        }
      }).then(res => console.log(res)).then(err => console.log(err));
    }
  }, [audiobuffer]);
  (0, _react.useEffect)(() => {
    navigator.mediaDevices.getUserMedia({
      audio: true
    }, () => {
      console.log("Permission Granted");
      setisBlocked(false);
    }, () => {
      console.log("Permission Denied");
      setisBlocked(true);
    });
  }, []);

  const start = () => {
    if (isBlocked) {
      console.log("Permission Denied");
    } else {
      Mp3Recorder.start().then(() => {
        setisRecording(true);
      }).catch(e => console.error("starterror", e));
    }
  };

  const stop = () => {
    Mp3Recorder.stop().getMp3().then(([buffer, blob]) => {
      const blobURL = URL.createObjectURL(blob); // let reader = new FileReader();

      console.log(buffer); // reader.readAsDataURL(blob);
      // reader.onloadend = function () {
      //   const base64 = reader.result;
      //   console.log(base64);
      // };

      setaudiobuffer(buffer);
      setblobURL(blobURL);
      setisRecording(false);
    }).catch(e => console.log(e));
  };

  return /*#__PURE__*/_react.default.createElement("div", {
    className: "container"
  }, !isRecording ? /*#__PURE__*/_react.default.createElement("div", {
    className: "record-div icon-div",
    onClick: start
  }, /*#__PURE__*/_react.default.createElement("span", null, /*#__PURE__*/_react.default.createElement(_FiberManualRecord.default, null))) : /*#__PURE__*/_react.default.createElement("div", {
    className: "stop-div icon-div",
    onClick: stop
  }, /*#__PURE__*/_react.default.createElement("span", null, /*#__PURE__*/_react.default.createElement(_Stop.default, null))), /*#__PURE__*/_react.default.createElement("audio", {
    controls: "controls",
    src: blobURL
  }));
}
},{}]},{},["pdwg"], null)
//# sourceMappingURL=/audio-recorder-popup.js.map