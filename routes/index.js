var AWS = require("aws-sdk");
// Set the region
AWS.config.update({ region: "us-east-1" });

// Create CloudWatch service object
var cw = new AWS.CloudWatch({ apiVersion: "2010-08-01" });

export default function routes(app, addon) {
  // Redirect root path to /atlassian-connect.json,
  // which will be served by atlassian-connect-express.
  app.get("/", (req, res) => {
    res.redirect("/atlassian-connect.json");
  });

  // This is an example route used by "generalPages" module (see atlassian-connect.json).
  // Verify that the incoming request is authenticated with Atlassian Connect.
  app.get("/audio-recorder", addon.authenticate(), (req, res) => {
    // Rendering a template is easy; the render method takes two params: the name of the component or template file, and its props.
    // Handlebars and jsx are both supported, but please note that jsx changes require `npm run watch-jsx` in order to be picked up by the server.
    // console.log("request", req);
    // console.log("request", req);
    res.render(
      "audio-recorder.hbs", // change this to 'hello-world.jsx' to use the Atlaskit & React version
      {
        title: "Atlassian Connectaskldjalsk",
        userAccountId: req.context.userAccountId,
        baseUrl: req.context.localBaseUrl,
        search: req._parsedUrl.search,
        token: req.context.token,
      }
    );
  });
  app.get("/custom", addon.authenticate(), (req, res) => {
    // Rendering a template is easy; the render method takes two params: the name of the component or template file, and its props.
    // Handlebars and jsx are both supported, but please note that jsx changes require `npm run watch-jsx` in order to be picked up by the server.
    // console.log("request", req);
    const params = {
      MetricData: [
        {
          MetricName: "PAGES_VISITED",
          Dimensions: [
            {
              Name: "UNIQUE_PAGES",
              Value: "URLS",
            },
          ],
          Unit: "None",
          Value: 1.0,
        },
      ],
      Namespace: "SITE/TRAFFIC",
    };

    cw.putMetricData(params, function (err, data) {
      if (err) {
        console.log("Error", err);
      } else {
        console.log("Success", JSON.stringify(data));
      }
    });

    res.render(
      "custom.hbs", // change this to 'hello-world.jsx' to use the Atlaskit & React version
      {}
    );
  });
}
